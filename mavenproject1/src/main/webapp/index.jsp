<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calcular</title>
    </head>
    <body>
            <h1>Calculadora de Ohm</h1>
            
            <form action="." method="POST">
                <p><input id="inpint" name="intensidad" required type="number" /><label for="inpint">Intensidad</label></p><br />
                <p><input id="inpres" name="resistencia" required type="number" /><label for="inpres">Resistencia</label></p><br />
                <input type="reset" value="Borrar campos">
                <input type="submit" />
            </form>
            <%
                if(((request.getParameter("intensidad") != null ) && (request.getParameter("intensidad") != "" )) && ((request.getParameter("resistencia") != null ) && (request.getParameter("resistencia") != "" )) ){
                    int intensidad=Integer.parseInt(request.getParameter("intensidad"));
                    int resistencia=Integer.parseInt(request.getParameter("resistencia"));
                    int voltaje = resistencia*intensidad;
                    int potencia = voltaje*intensidad;
            %>
            <br />
                    Voltaje: <input readonly value="<%= voltaje %>" /><br />
                    Potencia <input readonly value="<%= potencia %>" /><br />
                <% }
            %>
    </body>
</html>
